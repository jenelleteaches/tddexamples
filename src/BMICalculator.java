
public class BMICalculator {
	
	public String calculateBMI(double weight, double height) {
		double bmi = (weight) / (height * height);
		System.out.println("BMI: " + bmi);
		
		if (bmi > 18.5 && bmi < 25 ) {
			return "normal";
		}
		else if (bmi >= 25 && bmi < 30) {
			return "average";
		}
		else if (bmi >= 31 && bmi < 40) {
			return "fat";
		}
		else if (bmi >= 40) {
			return "severe fat";
		}
		else {
			return "aliens!";
		}
		
	}
}
