import static org.junit.Assert.*;

import org.junit.Test;

public class BMICalculatorTest {

	/*
	 * R1: BMI 18.5 - 25 = normal
	 * R2: BMI 26 - 30 - average
	 * R3: BMI 31-40 - fat
	 * R4: BMI 41+ -- severe fat
	 */
	
	// weight / (height * height)
	@Test
	public void testR1() {
		BMICalculator calc = new BMICalculator();
		String bmi = calc.calculateBMI(20, 1);
		assertEquals("normal", bmi);
	}
	
	@Test
	public void testR2() {
		BMICalculator calc = new BMICalculator();
		String bmi = calc.calculateBMI(28, 1);
		assertEquals("average", bmi);
	}

	@Test
	public void testR3() {
		BMICalculator calc = new BMICalculator();
		String bmi = calc.calculateBMI(32, 1);
		assertEquals("fat", bmi);
	}

	@Test
	public void testR4() {
		BMICalculator calc = new BMICalculator();
		String bmi = calc.calculateBMI(1200, 1);
		assertEquals("severe fat", bmi);	
	}
	
	@Test
	public void testWeirdNumber() {
		BMICalculator calc = new BMICalculator();
		String bmi = calc.calculateBMI(1, 1);
		assertEquals("aliens!", bmi);	
	}

}



